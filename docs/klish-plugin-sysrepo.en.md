# Module for working with Sysrepo in Klish3


## Overview

The klish-plugin-sysrepo module is designed to work with the configuration repository
[Sysrepo](#https://www.sysrepo.org/) from the program
[Klish version 3](#https://klish.libcode.org). Klish allows you to organize
A command line interface that contains only commands specified by the user at the
klish configuration. The Sysrepo repository uses a data modeling language
[YANG](#https://datatracker.ietf.org/doc/html/rfc6020) to form a schematic
of the stored configuration, and also stores the configuration itself. Typically, the stored
The configuration refers to an embedded system, such as a
to the router. The principle of configuration storage and management used in the
klish-plugin-sysrepo, similar to the approach used in Juniper routers.

The configuration scheme is described by YANG files, and accessing and editing items
configuration is performed using special commands available from klish.
All information about the scheme used by YANG klish-plugin-sysrepo gets from the
Sysrepo automatically.

The project contains the file `xml/sysrepo.xml` which, when added to the files
of the klish configuration, declares a new `VIEW` named "sysrepo". This `VIEW`
contains ready commands for editing the Sysrepo configuration. All work
module with Sysrepo is realized with the help of library functions Sysrepo and
libraries [`libyang`](#https://github.com/CESNET/libyang). Sysrepo projects,
libyang and klish are required to build and run the klish-plugin-sysrepo module.


## Configuration Storage

The Sysrepo system supports four configuration stores.

* `running` - current active configuration. The system state corresponds to
of this configuration. But this is not a configuration that the administrator edits.
* `candidate` is an editable configuration. "`candidate` to become a
by the current configuration. However, changes to `candidate` have no effect on the
the actual state of the system. Only by the fact that the administrator, a specialized
command, writes the contents of `candidate` to `running` will occur
changes to the system. That is, the changes are not applied one at a time, but "all at once".
In this way, the integrity and consistency of the current one is maintained
configurations. All commands described below work with the `candidate` configuration,
unless otherwise specified.
* `startup` - configuration saved on disk. Only this configuration
can survive a system reboot. In the case of the klish-plugin-sysrepo module
the `startup` and `running` storages are always paired. I.e. the module supports them
the condition is the same.
* `operational` - this repository is not directly related to the topic under discussion and
will no longer be mentioned here.


## KPath

Many commands use a "path" for configuration work. This is the path in YANG
scheme or rather in a value-filled configuration built by YANG
scheme. YANG schema is branching and multi-level. The circuit may contain
"lists" in which elements are identified by a "key". Path elements can be
be both YANG circuit nodes and keys, because the configuration branches to the
dependencies on these keys. The Sysrepo and libyang projects use
["XPath"](#https://www.w3schools.com/xml/xpath_intro.asp) to access the
configuration items. Even though XPath has an advanced syntax and
capabilities, it is not suitable for use in a command line interface.
XPath is quite complex and will not be intuitive to the operator. Therefore, for
klish-plugin-sysrepo uses simplified access to configuration items.
path variant. For definiteness, let's call it "KPath" (Klish path), so that we can then
refer to its format and not to be confused with other kinds of paths.

Path elements in KPath are separated from each other by spaces. Path elements
are YANG schema nodes, key values and `leaf-list` elements (see YANG).
If the key value or the `leaf-list` element contains spaces, they are enclosed in
in quotation marks.

Suppose there is such a piece of YANG circuitry:

```
module ttt {
  namespace "urn:ttt";
  prefix ttt;

  container test {
    list iface {
      key "name";

      leaf name {
        type string;
      }

      leaf comment {
        type string;
      }

      leaf type {
        type enumeration {
          enum ethernet;
          enum ppp;
          enum dummy;
        }
      }

    }
  }
}

```

The correct KPath to the element that defines the type of "interface" would be this:
`test iface eth0 type`, where "eth0" is the "iface" list key corresponding to the
to the "name" element.

The KPath to the comment field for interface "eth1" will be as follows:
`test iface eth1 comment`.

In commands where you need to enter the path to a configuration item, KPath will
such as described above. For the `set` command, which sets the value of the
configuration item, there is an extension of the KPath format, the so-called
"one-liners". Single lines allow you to set values for several elements
configuration (`leaf`) with a single line. The `set` command together with its arguments can
look like this:

```
# set test iface eth0 type ethernet comment "Comment with space"
```

One line specifies the value of the interface type and the comment. Here, actually,
path is interrupted by field values.


## Configuration commands


### Command `set`

The `set` command allows you to set the value of a configuration item.

```
# set <KPath> <value>
```

To select which element to assign a value to, the following is specified
[path KPath](#path-kpath) to that element and then the value itself. If the value
contains spaces, it must be enclosed in quotation marks.

Here, an element with KPath `test iface eth0 type` is assigned a value
`ethernet`:

```
# set test iface eth0 type ethernet
```

The `set` command can assign values to multiple configuration items at once.
Read about "one-line paths" in ["KPath KPath"](#path-kpath). Example
one-line text:

```
# set test iface eth0 type ethernet comment "Comment with space"
```

When specifying a KPath path to the `set` command, it is not necessary that all components of the path
already existed. The commands in these examples will work even if
interface "eth0" has not yet been created in the configuration. Missing path components
will be created automatically.


### Command `del`

The `del` command deletes the value of an item, or an entire configuration branch.

```
# del <KPath>
```

For example, you can delete a comment for an intreface:

```
# del test iface eth0 comment
```

And you can delete all settings for interface "eth0":

```
# del test iface eth0
```

### Command `edit`

When entering the configuration mode, the user is at the "root" of the tree
configuration, i.e. in the root node of the schema. And all the KPaths it specifies are
when working with the configuration, are full (absolute) paths from root
of the schematic to the element you are looking for. This is not always convenient if you are customizing a subsystem
with a high degree of nesting. Therefore, the `edit` command is provided, which
changes the user's current position in the configuration tree.

```
# edit <KPath>
```

Moving a user through the configuration tree is similar to moving through a
directories in the file system. The current path in the configuration tree is displayed
in the line preceding the user invitation, inside square brackets
after the word "edit".

```
[edit]
# edit test iface eth0
[edit test iface eth0]
#
```

After changing the current path, all KPath instructions become relative.
Regarding the current position in the configuration tree. For example, the KPath instructions in
`set` and `del` commands will be relative.

```
[edit]
# edit test iface eth0
[edit test iface eth0]
# set type ethernet
[edit test iface eth0]
# set comment "Comment with space"
[edit test iface eth0]
# del comment
```

The `edit` command allows you to change the current path only in the deepening direction
nesting. The `top`, `up`, `exit` commands allow you to move through the tree in the
in the opposite direction.

Note that you cannot specify a list (the "list" node) in the `edit` command
without specifying the key of one of its elements.


### Command `top`

The `top` command changes the user's current path in the configuration tree to
root element. If the user was already at the root of the tree, nothing is
is happening.

```
[edit test iface eth0]
# top
[edit]
# top
[edit]
#
```


### Command `up`

The `up` command changes the user's current path in the configuration tree by one
nesting level up. That is, it leaves the current path one level up. If
user is already in the root element of the tree, then nothing happens.

```
[edit test iface eth0]
# up
[edit test]
# up
[edit]
# up
[edit]
#
```


### Command `exit`

The `exit` command is similar to the `up` command with the difference that being executed in the
the root element of the tree, exits configuration mode.

```
[edit test iface eth0]
# exit
[edit test]
# exit
[edit]
# exit
>
```


### Command `insert`

In a YANG schema, a list (a "list" or "leaf-list" node) can be defined with a
with the "ordered-by user" parameter. This means that the sequence of elements
list has a value and is user adjustable. The `insert` command allows
to move items within such a list.

```
# insert <from_kpath> <first/last/before/after> [to_list_key]
```

Before you can move an item within a list, you must create it. Non-existent
elements cannot be moved.

The first parameter of the `insert` command specifies the KPath to the item that
is to be moved. Next, the keyword specifies the mode of movement:

* `first` - move the item to the beginning of the list.
* `last` - move the item to the end of the list.
* `before` - place the element immediately before the element specified next.
* `after` - place the element immediately after the element specified next.

For `before` and `after` modes, the third parameter specifies the key of that
element relative to which the position is selected. Note that here
is not the entire KPath, but only the key.

```
[edit]
# show
acl acl1
acl acl2
acl acl3
[edit]
# insert acl acl3 before acl1
[edit]
# show
acl acl3
acl acl1
acl acl2
[edit]
# insert acl acl2 first
[edit]
# show
acl acl2
acl acl3
acl acl1
```


### Command `commit`

The `commit` command confirms the configuration change and copies the contents of the
of the `candidate` configuration storage to the `running` storage. After successful
When the command is executed, the configuration that the user edited will be applied
in the system and will become valid. If the `candidate` configuration contains errors,
that can be checked by the YANG scheme, then the current configuration is changed
will not be displayed and an error message will appear on the screen.

When the configuration is written to the `running` storage, the configuration will also be written to the `running` storage
into the `startup` storage to "survive" a future system reboot.


### Command `check`

The `check` command checks the correctness of the currently edited configuration
relative to the YANG scheme. Successful execution of this command means that the command
`commit` can be executed without error.


### `reset` command

The `reset` command allows you to roll back any changes made during editing
configurations. The `candidate` configuration being edited will return to the `candidate` state
of a valid `running` configuration.


### Command `show`

The `show` command shows the current state of the configuration being edited.

```
# show [kpath]
```

The optional parameter accepts a KPath path. If KPath is specified, then on the
only the contents of the specified section will be displayed. By default, the command
`show` uses the user's current path in the configuration tree.

```
[edit]
# show
test
    iface eth0
        comment "Test desc"
        type ethernet
acl acl2
acl acl3
acl acl1
[edit]
# show test
iface eth0
    comment "Test desc"
    type ethernet
```


### Command `diff`

The `diff` command shows the difference between editable and valid
configurations.

```
[edit]
# show
test
    iface eth0
        comment "Test desc"
        type ethernet
acl acl2
acl acl3
acl acl1
#
# <There's a configuration change>
#
[edit]
# show
test
    iface eth0
        comment "New comment"
        type ethernet
acl acl2
acl acl1
acl acl4
## diff
test
    iface eth0
=        comment "New comment"
-acl acl3
+acl acl4
```

Newly added lines are indicated by the `+` symbol at the beginning of the line and green
color (if highlighting is enabled). Deleted lines are indicated by the `-` symbol in the
at the beginning of the line and in red (if highlighting is enabled). Modified lines
are indicated by the `=` symbol at the beginning of the line and yellow color (if enabled).
backlighting).


### Command `do`

The `do` command allows you to execute command mode commands without leaving the mode
configurations. The `do` command is used as a prefix and is immediately followed by a
the command mode command is entered.

```
[edit]
# do show running
```


## Module settings

When connecting the module to the klish system with the `PLUGIN` element, in the body of the element
`PLUGIN` you can specify settings for the module. The settings regulate some of the
features of the module behavior.


### Customizing `ShowBrackets`

The field can take values `y` and `n`. In case `y` is specified, the commands,
showing the configuration, display curly brackets highlighting sections and
nesting levels.

```
test {
    iface eth0 {
        comment "Test desc"
        type ethernet
    }
}
```

The display of curly braces is enabled by default.


### Customizing `ShowSemicolons`

The field can take values `y` and `n`. In case `y` is specified, the commands,
showing the configuration, display the `;` character at the end of lines with "leaves"
(`leaf' nodes).

```
test {
    iface eth0 {
        comment "Test desc";
        type ethernet;
    }
}
```

By default, the display of the `;` character is enabled.


### Customizing `KeysWithStatement`

There are two options for how to specify the keys of a list item. The first
option - sequentially set only key values without specifying the field name.
In this case, the order in which the keys are entered is important. This behavior will be used,
if the `KeysWithStatement = n` setting is specified. The second option is that the values can
should be set together with the key name. In this case, the order in which keys are entered is not
important. This behavior will be used if the setting is set to
`KeysWithStatement = y`.

Suppose that the keys of the "interfaces" list are the `name` and `type` fields.
Also assume that `FirstKeyWithStatement = y`. Setup command
"comment" for an interface will look like this if
`KeysWithStatement = y`:

```
# set test iface name eth0 type ethernet comment "Comment"
```

If `KeysWithStatement = n` then as follows:

```
# set test iface eth0 ethernet comment "Comment"
```

The default is `KeysWithStatement = y`.


### Setting `FirstKeyWithStatement`

Sometimes it is convenient to specify the keys of a list item by specifying the name of the key and its
value, but the first key, specify no key name, only the value.
Such behavior would be
used if `FirstKeyWithStatement = n`. This setting is used by
default. If `FirstKeyWithStatement = y`, then the first key, as well as all of the
subsequent, must be specified with the key name.

> If `KeysWithStatement = n`, then setting `FirstKeyWithStatement`.
> is ignored.

If `FirstKeyWithStatement = y`:

```
# set test iface name eth0 type ethernet
```

If `FirstKeyWithStatement = n`:

```
# set test iface eth0 type ethernet
```


### Setting `DefaultKeys`

According to the YANG standard, all keys in the list are mandatory. Values
defaults (`default` directive) for keys are ignored. However, if the list has
many keys, it may be convenient to omit some of them when typing. To
to use this behavior, you must specify the setting `DefaultKeys = y`. This
setting acts in conjunction with the YANG extension `klish:default` defined in the
`klish.yang` file. If in a YANG file using the `klish:default` extension
is specified as the default value for the key, then if the administrator has not entered a
value of this key explicitly, then the key is mapped to the default value.

Note that the extension does not violate the YANG standard, regarding,
that it is mandatory that all keys be entered. Since the default values are
are only used during the query generation phase, inside the plowin. YANG query
contains all the keys.

> If the `FirstKeyWithStatement = n` setting is used and the first
> the key has a default value, then the setting stops working and the key
> must be entered with the key name, as must all subsequent keys.

> The `DefaultKeys` setting is ignored if `KeysWithStatements = n`.

Example of a YANG file using default keys:

```
...
import klish { prefix "klish"; }
...
list list {
  key "key1 key2 key3";
  leaf key1 {
    description "First key";
    type string;
    klish:default "def1";
  }
  leaf key2 {
    description "Second key";
    type string;
    klish:default "def2";
  }
  leaf key3 {
    description "Third key";
    type string;
  }
  leaf el1 {
    description "First el";
    type string;
    default "el1-def";
  }
  leaf el2 {
    description "Second el";
    type string;
  }
}
```

An administrator can enter the following command by omitting the `key1` and `key2` keys,
since they have default values:

```
# set list key3 nnn el1 mmm
# show
...
list key1 def1 key2 def2 key3 nnn {
  el1 mmm
}
...
```


### Setting `ShowDefaultKeys`

If the setting value is set to `n` and the key value is
of the list (in `DefaultKeys = y` mode) is the same as the default value, then
such a key will not be shown on the screen when the configuration is output.


### Setting `HidePasswords`

If passwords or password hashes are stored in the configuration, then this information is
it is desirable not to show on the screen. To hide field values when displaying on the screen
screen, the `HidePasswords = y` setting is used. This setting is effective
together with the YANG extension `klish:password` defined in the file
`klish.yang`.

YANG module, wherein one of the fields is labeled "password".

```
...
import klish { prefix "klish"; }
...
leaf pass {
  type string;
  klish:password;
}
...
```

Then, if the administrator wants to see the contents of the configuration, the
will see the following:

```
# show
...
pass <hidden>
...
```

The field value is replaced with the string `<hidden>`.


### Setting `Colorize`

The field can take values `y` and `n`. In the case when `y` is given, then at
displaying configuration or configuration changes, characteristic elements of the
will be highlighted in color.

> Color selection is now implemented only in the `diff` command. New elements
> are highlighted in green, deleted are highlighted in red, and those that have changed their value -
> yellow.

Some terminals, mostly obsolete terminals, may not support colors. Therefore
In some cases it may be useful to disable the `Colorize` setting.


### Setting `Indent`

The field takes a numeric value. Sets the amount of indentation for each level
nesting. Measured in the number of "space" characters at the beginning of a line. By
defaults to `2`.


### Configuring `EnableNACM`

By default, the NACM module (ietf-netconf-acm) is considered a service module and its configuration is
impossible. The items are hidden from the user. Setting `EnableNACM = y`.
forces the NACM module to be treated as a normal module. User
gets the ability to control the NACM.


### Customizing `Oneliners`

If the setting is `Oneliners = y` (used by default), and the item has less than
two descendants, then this element and its only descendants will be shown in a single
line. Opening and closing brackets will not be shown. Elements, in general
with no descendants will also be shown without opening and closing brackets.


### Example of module configuration

```
<PLUGIN name="sysrepo">
    ShowBrackets = y
    ShowSemicolons = y
    KeysWithStatement = y
    FirstKeyWithStatement = n
    Colorize = y
    Indent = 2
    HidePasswords = y
    DefaultKeys = y
    EnableNACM = n
</PLUGIN>.
```
